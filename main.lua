-- general imports
Lume = require "lib.lume"

-- hump imports
Camera = require "lib.hump.camera"
Class = require "lib.hump.class"
Gamestate = require "lib.hump.gamestate"
Signal = require "lib.hump.signal"
Timer = require "lib.hump.timer"

-- debug imports
Inspect = require "lib.inspect"
Lovebird = require "lib.lovebird"

-- engine imports
require "src.engine.oscillator"

function love.load()
    love.graphics.setDefaultFilter("nearest", "nearest")
    love.graphics.setLineStyle("rough")

    oscillator = Oscillator(18)
    x = 100
    rotation = 0
    scale = 1
    test = love.graphics.newImage("test.png")
end

function love.update(dt)
    scale = oscillator:get(10)

    if love.keyboard.isDown("space") then
        x = x + 1.5
        oscillator:update(dt)
        rotation = oscillator:get(0.1)
    end
end

function love.draw()
    love.graphics.setBackgroundColor(1, 1, 0)
    love.graphics.draw(test, x, 200, rotation, scale, scale, 50, 50)
end
