Oscillator = Class {}

function Oscillator:init(modifier)
    self.time = 0
    self.modifier = modifier or 1
end

function Oscillator:update(dt)
    self.time = self.time + dt * self.modifier
end

function Oscillator:get(cap)
    if not cap then cap = 1 end
    return cap * math.sin(self.time)
end
