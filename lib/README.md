## LÖVE
- [LÖVE](https://love2d.org/) `v11.1`

## Libraries
- [hump](https://hump.readthedocs.io/en/latest/index.html) `v0.4-2`
- [inspect](https://github.com/kikito/inspect.lua) `v3.1.1`
- [lovebird](https://github.com/rxi/lovebird) `v0.4.3`
- [lume](https://github.com/rxi/lume/) `v2.3.0`
